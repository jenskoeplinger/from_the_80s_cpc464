# From the 80's: Schneider CPC464 code snippets

My little code projects, as published in "CPC Schneider International" magazine.

Computer: "Schneider CPC464" (German version of the British "Amstrad CPC464")

How to run:

* Get an [Amstrad CPC emulator](https://en.wikipedia.org/wiki/List_of_computer_system_emulators)
    * For example: http://crocods.org/web/ (working as  of 12 Dec 2021)
* Insert disk using one of the `*.dsk` files here
* optional: type command `CAT`, then ENTER, to see the list of files on disk
* Run the start command below.

List of `dsk` files:

```text
Disk          Start command
-----------------------------------
Cave          RUN"CAVE.BAS
H'Cardin      RUN"H'CARDIN.BAS
Mad Maze      RUN"MADMAZE.BAS
Pecunia       RUN"PECUNIA.BAS
Turris        RUN"TURRIS.BAS
Uniter        RUN"UNITER.BAS
```

... there are more, but I don't remember or find them ...

